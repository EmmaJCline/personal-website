import * as astar from '../pages/blog/astar-csharp.mdx'
import * as dstructError from '../pages/blog/dstruct-error-list-map-tree.mdx'
import * as dstructPoster from '../pages/blog/dstruct-poster.mdx'
import * as dstructSet from '../pages/blog/dstruct-set-multiset-stack.mdx'
import * as dstructTables from '../pages/blog/dstruct-tables-queues.mdx'

export default [astar, dstructError, dstructPoster, dstructSet, dstructTables]
